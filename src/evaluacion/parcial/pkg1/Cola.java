
package evaluacion.parcial.pkg1;

// cola circular 3

import static evaluacion.parcial.pkg1.Pila.cima;
import static evaluacion.parcial.pkg1.Pila.cimaaux;
import java.util.Random;

public class Cola{
	String[] ArregloCola;
        int primero=-1,ultimo=-1; //declaración cola principal
        String[] ArregloColaux; 
        int primeroaux=-1,ultimoaux=-1; //declarar cola auxiliar
	int Cant_elem=10;
        int Cant_elem2=10; //tamaño del arreglo cola
        
	//Constructor
	Cola(){
		ArregloCola=new String[Cant_elem];
                ArregloColaux =new String[Cant_elem2];
                primeroaux=-1;
		ultimoaux=-1;
		primero=-1;
		ultimo=-1;
                
                LlenarCola();
                
	}
        
    private void LlenarCola() 
    {
        Random random= new Random();
        int numero;
        
        for (int i = 0; i < 10; i++) {
            numero = random.nextInt((50 - 1) + 1 ) +1;
                Ingresar(String.valueOf(numero));
        }
        
         
    }


        public boolean ColaLlena(){
            //primero y último en los extremos
            if ((primero==0)&&(ultimo==Cant_elem-1)) 
                return(true);
            if (ultimo+1==primero) //último más uno alcanza a primero
                return (true);
            return(false);
        }

	//Encola un elemento
	public void Ingresar(String x)
        {
		if(ColaVacia()) 
                {
                        primero++;
			ultimo++;
                        //en la posición de último en ArregloCola se
                        //guarda el dato X
			ArregloCola[ultimo]=x; 
         	}
		else{
			if(ColaLlena())
				System.out.println("No hay campo");
			else
                            if (ultimo==Cant_elem-1)
                                ultimo=0;
                            else
                                ultimo++;
                        ArregloCola[ultimo]=x;
			}
	}
       

    //Desencola un elemento
	public String Avanzar(){
            String dato=null;
		if(ColaVacia())
			System.out.println("No hay Elementos");
		else{
                    dato=ArregloCola[primero];
                    if (primero == ultimo){
                        primero=-1;
                        ultimo=-1;
                        }
                    else{
                        if (primero==Cant_elem-1)
                            primero=0;
                        else
                            primero++;
                    }
		}
            return(dato);
	}

    public void Ingresaraux(String x)
        {
		if(ultimoaux==-1)
                {
			ultimoaux++;
                        primeroaux++;
			ArregloColaux[ultimoaux]=x;
		}
		else{
			ultimoaux++;
			if(ultimoaux==Cant_elem2)
				System.out.println("No hay campo");
			else
				ArregloColaux[ultimoaux]=x;
			}
	}

    public String Avanzaraux(){
            String dato=null;
		if(VaciaColaux())
			System.out.println("No hay Elementos");
		else{
                    dato=ArregloColaux[primeroaux];
                    if (primeroaux == ultimoaux){
                        primeroaux=-1;
                        ultimoaux=-1;
                        }
                    else{
                        primeroaux++;
                    }
		}
            return(dato);
	}

	//Retorna si esta vacia la cola
	public boolean VaciaColaux(){
		return (ultimoaux==-1 && primeroaux==-1);
	}

	//Retorna si esta vacia la cola 2
	public boolean ColaVacia(){
		return (ultimo==-1 && primero==-1);
	}
        
       public void Imprimir(){
        String desplazar2;
        if(ColaVacia())
            System.out.println("No hay Elementos");
        else{
            while(!ColaVacia()){
                desplazar2=Avanzar();
                
                Ingresaraux(desplazar2);
            }
            while(!VaciaColaux()){
                desplazar2=Avanzaraux();
                System.out.print(desplazar2+" - ");
                Ingresar(desplazar2);
            }
            System.out.println("");
	}
        
        
    }
       
       
    public boolean relacionarPila(Pila pila){
        boolean bandera = false;
        String numero2;
        if(ColaVacia())
            System.out.println("NO HAY NINGUN ELEMENTO");
        else{
            while(!ColaVacia()){
                numero2=Avanzar();
                Ingresaraux(numero2);
            }
            label_while: while(!VaciaColaux()){
                numero2=Avanzaraux();
                String numero1,fin=" ";
                if (cima!=-1){ 
                    do {
                        numero1 = pila.Pop();
                        if (numero1.equals(numero2)){
                            System.out.println(numero2 + " SE RELACIONA "  + numero1);
                            System.out.println("SON IGUALES");
                            System.out.println("FIN DEL RECORRIDO");
                            bandera = true;
                            break label_while;
                        }else{
                            System.out.println(numero2 + " SE RELACIONA "  + numero1);
                            System.out.println("NO SON IGUALES");
                        }  
                        fin=fin+numero1+" - "; 
                        pila.PushAux(numero1);            
                  }while(cima!=-1);
                  do {
                      numero1=pila.PopAux();
                      pila.Push(numero1);
                  }while(cimaaux!=-1);
                }
                else {
                    System.out.println("La pila esta vacía");
                }              
                Ingresar(numero2);
            }
            System.out.println("");
	}
        return bandera;
    }
}
		
